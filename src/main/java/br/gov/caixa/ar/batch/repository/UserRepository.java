package br.gov.caixa.ar.batch.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.caixa.ar.batch.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {
}
