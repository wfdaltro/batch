package br.gov.caixa.ar.batch.batch;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import br.gov.caixa.ar.batch.model.User;

@Component
public class Processor implements ItemProcessor<User, User> {

    private static final Map<String, String> DEPT_NAMES =
            new HashMap<>();

    public Processor() {
        DEPT_NAMES.put("001", "Departamento1");
        DEPT_NAMES.put("002", "Departamento2");
        DEPT_NAMES.put("003", "Departamento3");
    }

    @Override
    public User process(User user) throws Exception {
        String deptCode = user.getDept();
        String dept = DEPT_NAMES.get(deptCode);
        user.setDept(dept);
        user.setTime(new Date());
        System.out.println(String.format("Convertido de [%s] para [%s]", deptCode, dept));
        return user;
    }
}
